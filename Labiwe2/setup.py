#!/usr/bin/env python 2.7
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(name='maxonrm', packages=find_packages(exclude=['test_trash']),
      entry_points={'console_scripts':
                        [
                              'maxonrm = maxonrm.arg:main',
                              'config = maxonrm.con_parser:main'
                        ]
      },
      description='Maxim rm', author='maxim')


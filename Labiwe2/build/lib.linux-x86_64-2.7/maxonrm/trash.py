import os
import datetime
import shutil
import re
from maxonrm.ver import Checker

# from maxonrm.policy import Policy
from restore_policy import RestorePolicy
from autodelete import autodeleted_by_policy
from rej import run_del
from rej import run_rest

class Trash(object):

    def __init__(self, trash_path, max_size, size, policy, mode, date, restore_policy):

        # trash_path = '/home/maxim/PycharmProjects/TRASH'

        self.TRASH_PATH = trash_path
        self.INFO_PATH = self.TRASH_PATH + '/info'
        self.FILES_PATH = self.TRASH_PATH + '/files'
        self.MAX_SIZE = max_size
        self.POLICY = policy
        self.DATE = date
        self.SIZE = size
        self.SILENT_MOD = False
        self.DRY_RUN_MOD = False
        self.MODE = mode    # interactive, force
        self.RESTORE_POLICY = restore_policy     # replace, ask

        self.make_dir(self.TRASH_PATH)
        self.make_dir(self.INFO_PATH)
        self.make_dir(self.FILES_PATH)

        autodeleted_by_policy(self)

        Checker.update(self.TRASH_PATH)

    @staticmethod
    def make_dir(path):
        try:
            os.mkdir(path)
        except OSError:
            pass

    def dry_run_switcher(self):
        self.DRY_RUN_MOD = not self.DRY_RUN_MOD

    def delete_file(self, dir_path, file_name):

            folders, file_name = os.path.split(file_name)

            path = os.path.join(dir_path if folders == '' else folders, file_name)

            if os.path.lexists(path):
                if not Checker.trash_check(self.FILES_PATH, path):
                    if not self.DRY_RUN_MOD:

                        run_del(file_name, path, self)
                        print self.TRASH_PATH

                else:
                    raise Exception("file %s is already in trash" % file_name)
            else:
                raise IOError("Don't found file or directory %s" % file_name)

    def restore_file(self, file_name):
            old_path = os.path.join(self.FILES_PATH, file_name)
            if os.path.lexists(old_path):
                with open(os.path.join(self.INFO_PATH, file_name + ".file_info"), "r") as file_info:
                    new_path = file_info.readline().strip("\n")
                    if not self.DRY_RUN_MOD:
                        run_rest(file_name, new_path, old_path, self)
            else:
                raise IOError("file %s don't found" % file_name)

    def remove_from_trash(self, file_name):
        file_path = os.path.join(self.FILES_PATH, file_name)
        info_path = os.path.join(self.INFO_PATH, file_name + ".file_info")
        if os.path.lexists(file_path):
            if not self.DRY_RUN_MOD:
                if os.path.isdir(file_path):
                    shutil.rmtree(file_path)
                    os.remove(info_path)
                else:
                    os.remove(file_path)
                    os.remove(info_path)
        else:
            raise IOError("%s don't founded" % file_name)

    def clean_trash(self):
        # self.look_trash()
        if not self.DRY_RUN_MOD:
            shutil.rmtree(self.INFO_PATH)
            shutil.rmtree(self.FILES_PATH)
            self.make_dir(self.INFO_PATH)
            self.make_dir(self.FILES_PATH)

    def look_trash(self):
        files = os.listdir(self.INFO_PATH)
        for file_something in files:
            file_name = os.path.join(self.INFO_PATH, file_something)
            with open(file_name, "r") as file_info:
                file_path = file_info.readline()
            print ".".join(file_something.split(".")[:-1]), "\t", file_path

    def remove_by_re(self, path, re_ex):
        files_or_dirs = os.listdir(path)
        for file_or_dir in files_or_dirs:
            if re.search(re_ex, file_or_dir):
                self.delete_file(path, file_or_dir)
            else:
                if os.path.isdir(os.path.join(path, file_or_dir)):
                    self.remove_by_re(os.path.join(path, file_or_dir), re_ex)

    def restore_by_re(self, re_ex):
        files_or_dirs = os.listdir(self.FILES_PATH)
        for file_or_dir in files_or_dirs:
            if re.search(re_ex, file_or_dir):
                self.restore_file([file_or_dir])


if __name__ == '__main__':
    t = Trash('/home/maxim/PycharmProjects/TRASH', 15, 150, 'size', 'force', '22', 'ask')
    # t.look_trash()
    # t.restore_file('5')
    t.delete_file('/home/maxim/', '123')

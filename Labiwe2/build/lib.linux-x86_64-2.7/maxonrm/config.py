import ConfigParser
import json
import os


class Conf(object):

    @staticmethod
    def init_config():
        config = ConfigParser.RawConfigParser()

        config.add_section('sc1')
        config.set('sc1', 'trash_path', '/home/maxim/PycharmProjects/TRASH')
        config.set('sc1', 'policy', 'time')
        config.set('sc1', 'max_size', '150000')
        config.set('sc1', 'size', '15000000')
        config.set('sc1', 'date', '2017-04-22 02:48:02.020848')
        config.set('sc1', 'mode', 'force')
        config.set('sc1', 'restore_policy', 'replace')

        json_conf = dict()
        json_conf['trash_path'] = '/home/maxim/PycharmProjects/TRASH'
        json_conf['policy'] = 'time'
        json_conf['max_size'] = 1500000
        json_conf['size'] = 150000000
        json_conf['date'] = '2017-04-22 02:48:02.020848'
        json_conf['mode'] = 'force'
        json_conf['restore_policy'] = 'replace'

        config_path = '~/PycharmProjects'

        with open(os.path.join(os.path.expanduser(config_path), 'config.cfg'), 'wb') as config_file:
            config.write(config_file)

        with open(os.path.join(os.path.expanduser(config_path), 'config.json'), 'wb') as json_config:
            json.dump(json_conf, json_config, indent=0)

    @staticmethod
    def smth_config(option, value):
        config = ConfigParser.SafeConfigParser()

        config.add_section('sc1')
        config.set('sc1', str(option), str(value))

        json_conf = dict()
        json_conf[str(option)] = value

        with open('config.cfg', 'wb') as config_file:
            config.write(config_file)

        with open('config.json', 'wb') as json_config:
            json.dump(json_conf, json_config, indent=0)

    # @staticmethod
    # def make_config():
    #     config = ConfigParser.RawConfigParser()
    #     json_conf = dict()
    #
    #     config.add_section('sc1')
    #     trash_path = raw_input("Enter trash_path or skip(press ENTER) for default value")
    #     if trash_path == '':
    #         config.set('sc1', 'trash_path', '/home/maxim/PycharmProjects/Labiwe2/trash')
    #         json_conf['trash_path'] = '/home/maxim/PycharmProjects/Labiwe2/trash'
    #     else:
    #         config.set('sc1', 'trash_path', trash_path)
    #         json_conf['trash_path'] = trash_path
    #
    #     max_size = raw_input("Enter max_size or skip(press ENTER) for default value")
    #     if max_size == '':
    #         config.set('sc1', 'max_size', 11000)
    #         json_conf['max_size'] = 11000
    #     else:
    #         config.set('sc1', 'max_size', max_size)
    #         json_conf['max_size'] = max_size
    #
    #     policy = raw_input("Enter policy(time, size, name) or skip(press ENTER) for default value")
    #     if policy == '':
    #         config.set('sc1', 'policy', "time")
    #         json_conf['policy'] = "time"
    #     else:
    #         config.set('sc1', 'policy', policy)
    #         json_conf['policy'] = policy
    #
    #     with open('~/PycharmProjects/pyson/Labiwe2/config.cfg', 'wb') as config_file:
    #         config.write(config_file)
    #
    #     with open('~/PycharmProjects/pyson/Labiwe2/config.json', 'wb') as json_config:
    #         json.dump(json_conf, json_config, indent=0)

if __name__ == '__main__':
    Conf.init_config()
    # Conf.smth_config('policy', 'size')

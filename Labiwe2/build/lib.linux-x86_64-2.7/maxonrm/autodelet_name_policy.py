import os


class AutoTimePolicy(object):

    @staticmethod
    def autodelete_by_time(trash=None):
        trash_files_and_dirs = os.listdir(trash.FILES_PATH)
        for file_or_dir in trash_files_and_dirs:
            with open(os.path.join(trash.INFO_PATH, file_or_dir + ".file_info"), "r") as file_info:
                new_path = file_info.readline().strip("\n")
                time = file_info.readline()
            if time > trash.DATE:
                trash.remove_from_trash(file_or_dir)

    @staticmethod
    def delete_by_time(file_name, trash=None):

    def run(self, trash=None):
        self.autodelete_by_time(trash)

from size_policy import SizePolicy
from name_policy import NamePolicy
from time_policy import TimePolicy


class Policy(object):

    @staticmethod
    def delete_by_policy(trash, count, flag):
        if flag == "time":
            TimePolicy.run(trash, count)
        elif flag == "name":
            NamePolicy.run(trash, count)
        elif flag == "size":
            SizePolicy.run(trash, count)
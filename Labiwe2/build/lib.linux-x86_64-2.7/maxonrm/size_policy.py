import os


class SizePolicy(object):

    @staticmethod
    def delete_by_size(trash, count):
        file_list = []
        trash_files_and_dirs = os.listdir(trash.FILES_PATH)
        for file_or_dir in trash_files_and_dirs:
            path = os.path.join(trash.FILES_PATH, file_or_dir)
            size = os.path.getsize(path)
            file_list.append([size,  file_or_dir])
        file_list.sort()

        try:
            for i in xrange(count):
                trash.remove_from_trash(file_list[i][1])
        except IndexError:
            pass

    @staticmethod
    def run(trash=None, count=5):
        SizePolicy.delete_by_size(trash, count)
import argparse
from trash import Trash
import os
from maxonrm.config import Conf
import ConfigParser
import logging
import json


logging.basicConfig(filename='lo0g.txt', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


def main():
    config = ConfigParser.RawConfigParser()
    config.read("/home/maxim/PycharmProjects/config.cfg")

    trash_path = config.get('sc1', 'trash_path')
    max_size = config.getint('sc1', 'max_size')
    size = config.getint('sc1', 'size')
    policy = config.get('sc1', 'policy')
    restore_policy = config.get('sc1', 'restore_policy')
    mode = config.get('sc1', 'mode')
    date = config.get('sc1', 'date')
    #
    # print trash_path, max_size, max_count, policy, silent_mod, dry_run

    t = Trash(trash_path, max_size, size, policy, mode, date, restore_policy)

    log = logging.getLogger()

    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--look", action="store_true", required=False)
    parser.add_argument("-c", "--clean", action="store_true", required=False)
    parser.add_argument('-d', "--delete", nargs="+", action="store", help="deletion", required=False)
    parser.add_argument('-dr', "--remove_by_re", action="store", help="deletion by regex", required=False)
    parser.add_argument('-r', "--restore", nargs="+", action="store", help="restoring files", required=False)
    parser.add_argument('-dron', "--dry_run_on", action="store_true", help="dry run mod on", required=False)
    parser.add_argument('-son ', "--silent_on", action="store_true", help="silent mod on", required=False)
    parser.add_argument('-dt', "--delete_from_trash", nargs="+",
                        action="store", help="delete files from trash", required=False)
    parser.add_argument('-dc', "--default_config", action="store_true", help="use default config", required=False)
    parser.add_argument('-cc', "--custom_config", action="store_true", help="use custom config", required=False)

    # parser.add_argument('-p', '--path', action="store_true", help='path of trash')
    # parser.add_argument('-s', '--size', action="store_true", help='maximal size of trash')
    # parser.add_argument('-ms', '--max_size', action="store_true", help='maximal size of file in trash.')
    # parser.add_argument('-da', '--date', action="store_true", help='maximal time for autodelete files in trash')
    # parser.add_argument('-po', '--policy', action="store_true", help='autodelete policy(time or size)')
    # parser.add_argument('-rp', '--restore_policy', action="store_true", help='restore policy(replace or ask)')
    # parser.add_argument('-m', '--mode', action="store_true", help='mode of working(force, interactive')
    # parser.add_argument('-lc', '--look_config', action="store_true", help='list config')

    args = parser.parse_args()
    #
    # json_conf = dict()
    # config_path = '~/PycharmProjects'
    #
    # if args.path:
    #     config.set('sc1', 'trash_path', args.path)
    #     json_conf['trash_path'] = args.path
    # else:
    #     config.set('sc1', 'trash_path', '/home/maxim/PycharmProjects/TRASH')
    #     json_conf['trash_path'] = '/home/maxim/PycharmProjects/TRASH'
    # if args.size:
    #     config.set('sc1', 'size', args.size)
    #     json_conf['size'] = args.size
    # else:
    #     config.set('sc1', 'size', '150')
    #     json_conf['size'] = '150'
    # if args.max_size:
    #     config.set('sc1', 'max_size', args.max_size)
    #     json_conf['max_size'] = args.max_size
    # else:
    #     config.set('sc1', 'max_size', '15')
    #     json_conf['max_size'] = '15'
    # if args.date:
    #     config.set('sc1', 'date', args.date)
    #     json_conf['date'] = args.date
    # else:
    #     config.set('sc1', 'date', '2017-04-22 02:48:02.020848')
    #     json_conf['date'] = '2017-04-22 02:48:02.020848'
    # if args.policy:
    #     config.set('sc1', 'policy', args.policy)
    #     json_conf['policy'] = args.policy
    # else:
    #     config.set('sc1', 'policy', 'size')
    #     json_conf['policy'] = 'size'
    # if args.restore_policy:
    #     config.set('sc1', 'restore_policy', args.restore_policy)
    #     json_conf['restore_policy'] = args.restore_policy
    # else:
    #     config.set('sc1', 'restore_policy', 'replace')
    #     json_conf['restore_policy'] = 'replace'
    # if args.mode:
    #     config.set('sc1', 'mode', str(args.mode))
    #     json_conf['mode'] = args.mode
    # else:
    #     config.set('sc1', 'mode', 'force')
    #     json_conf['mode'] = 'force'
    #
    # with open(os.path.join(os.path.expanduser(config_path), 'config.cfg'), 'wb') as config_file:
    #     config.write(config_file)
    # with open(os.path.join(os.path.expanduser(config_path), 'config.json'), 'wb') as config_json:
    #     json.dump(json_conf, config_json, indent=0)

    if args.default_config:
        Conf.init_config()

    if args.custom_config:
        Conf.make_config()

    if args.dry_run_on:
        t.dry_run_switcher()

    if args.silent_on:
        log.setLevel(logging.CRITICAL)

    if args.look:
        t.look_trash()
        logging.info('trash looked')

    if args.delete_from_trash is not None:
        for file_or_dir in args.delete_from_trash:
            try:
                t.remove_from_trash(file_or_dir)
                logging.info('%s removed from trash', file_or_dir)
            except Exception as e:
                logging.error(e)

    if args.clean:
        t.clean_trash()
        logging.info('trash cleaning')

    if args.remove_by_re is not None:
        t.remove_by_re(os.getcwd(), args.remove_by_re)

    if args.delete is not None:
        for file_or_dir in args.delete:
            try:
                t.delete_file(os.getcwd(), file_or_dir)
                logging.info('%s successful placed into trash', file_or_dir)
            except Exception as e:
                logging.error(e)

    if args.restore is not None:
        for file_or_dir in args.restore:
            try:
                t.restore_file(file_or_dir)
                logging.info('%s successful restored from trash', file_or_dir)
            except Exception as e:
                logging.error(e)

if "__main__" == __name__:
    main()

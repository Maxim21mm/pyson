import logging

logging.basicConfig(filename='log.txt', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


# def changelog():
#     log = logging.getLogger()
#     log.setLevel(logging.CRITICAL)


# def decor(dry_run=False):
def logger(func):
    def wrapper(*args, **kwargs):
        try:
            logging.info(func(*args, **kwargs))
            # logging.info("function is %s, with args %s and kwargs %s", func.__name__, args[1:], kwargs)
        except Exception as e:
            logging.error(e)
    return wrapper
    # return logger

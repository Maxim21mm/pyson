import os


class NamePolicy(object):

    @staticmethod
    def delete_by_name(trash, count):
        file_list = os.listdir(trash.FILES_PATH)
        file_list.sort()
        try:
            for i in xrange(count):
                trash.remove_from_trash(file_list[i])
        except IndexError:
            pass

    @staticmethod
    def run(trash=None, count=5):
        NamePolicy.delete_by_name(trash, count)
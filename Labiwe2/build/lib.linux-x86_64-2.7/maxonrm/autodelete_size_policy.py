import os


def autodelete_by_size(trash=None):
    trash_files_and_dirs = os.listdir(trash.FILES_PATH)
    for file_or_dir in trash_files_and_dirs:
        size = os.path.getsize(os.path.join(trash.FILES_PATH, file_or_dir))
        if size > trash.SIZE:
            trash.remove_from_trash(file_or_dir)

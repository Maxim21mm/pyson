from autodelete_time_policy import autodelete_by_time
from autodelete_size_policy import autodelete_by_size


def autodeleted_by_policy(trash=None):
    if trash.POLICY == 'time':
        autodelete_by_time(trash)
    elif trash.POLICY == 'size':
        autodelete_by_size(trash)

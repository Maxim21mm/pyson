from unittest import TestCase
from trash import Trash
import os
import shutil


class TestTrash(TestCase):

    def setUp(self):
        self.path = os.path.expanduser("~/PycharmProjects/trash_test")
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        self.trash = Trash()
        self.trash_path = self.trash.TRASH_PATH
        self.file_path = self.trash.FILES_PATH
        self.info_path = self.trash.INFO_PATH

    def tearDown(self):
        shutil.rmtree(self.path)

    def test_make_dir(self):
        self.trash.make_dir(self.trash.FILES_PATH)
        self.assertTrue(os.path.exists(self.trash.FILES_PATH))

    def test_delete_file(self):
        file_path = os.path.join(self.path, 'gg')
        with open(file_path, 'w') as tmp_file:
            tmp_file.write('abc')

        folders, file_name = os.path.split(file_path)
        self.trash.delete_file(folders, file_name)

        self.assertTrue(os.path.exists(os.path.join(self.file_path, "gg")))
        self.assertTrue(os.path.exists(os.path.join(self.info_path, "gg.file_info")))
        self.assertFalse(os.path.exists(file_path))

    def test_restore_file(self):
        file_path = os.path.join(self.file_path, "gg_r")
        with open(file_path, "w") as restore_file:
            restore_file.write('123')

        info_path = os.path.join(self.info_path, "gg_r.file_info")
        with open(info_path, "w") as info:
            info.write(self.path + '/gg_r')

        self.trash.restore_file("gg_r")

        self.assertTrue(os.path.exists(os.path.join(self.path, "gg_r")))
        self.assertFalse(os.path.exists(os.path.join(self.file_path, "gg_r")))
        self.assertFalse(os.path.exists(os.path.join(self.info_path, "gg_r.file_info")))

    def test_remove_from_trash(self):
        file_path = os.path.join(self.trash.FILES_PATH, "gg3.txt")
        with open(file_path, "w"):
            pass

        info_path = os.path.join(self.trash.INFO_PATH, "gg3.txt.file_info")
        with open(info_path, "w"):
            pass

        self.trash.remove_from_trash("gg3.txt")

        self.assertFalse(os.path.exists(os.path.join(self.trash.FILES_PATH, "gg3.txt")))
        self.assertFalse(os.path.exists(os.path.join(self.trash.INFO_PATH, "gg3.txt.file_info")))

    def test_clean_trash(self):
        file_path = os.path.join(self.file_path, "gg3.txt")
        with open(file_path, "w"):
            pass

        info_path = os.path.join(self.info_path, "gg3.txt.file_info")
        with open(info_path, "w"):
            pass

        self.trash.clean_trash()

        files_list = os.listdir(self.file_path)
        info_list = os.listdir(self.info_path)
        self.assertTrue(len(files_list) == 0)
        self.assertTrue(len(info_list) == 0)

    def test_remove_by_re(self):
        file1_path = os.path.join(self.path, "aaaaAAA")
        with open(file1_path, "w") as f1:
            f1.write('file1')

        file2_path = os.path.join(self.path, "bbb")
        with open(file2_path, "w") as f2:
            f2.write('file2')

        self.trash.remove_by_re(self.path, "aAAA")

        self.assertTrue(os.path.exists(os.path.join(self.trash.FILES_PATH, "aaaaAAA")))
        self.assertTrue(os.path.exists(os.path.join(self.trash.INFO_PATH, "aaaaAAA.file_info")))
        self.assertFalse(os.path.exists(os.path.join(self.path, "aaaaAAA")))
        self.assertTrue(os.path.exists(os.path.join(self.path, "bbb")))


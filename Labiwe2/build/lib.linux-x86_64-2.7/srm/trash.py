import os
import datetime
import shutil
import logging
import re
from look import logger
from ver import Checker
import ConfigParser
from policy import Policy


class Trash(object):

    def __init__(self):

        # config = ConfigParser.RawConfigParser()
        # config.read("/home/maxim/PycharmProjects/pyson/Labiwe2/srm/config.cfg")
        #
        # self.TRASH_PATH = config.get('sc1', 'trash_path')
        # self.INFO_PATH = self.TRASH_PATH + '/info'
        # self.FILES_PATH = self.TRASH_PATH + '/files'
        # self.MAX_SIZE = config.getint('sc1', 'max_size')
        # self.MAX_COUNT = config.getint('sc1', 'max_count')
        # self.POLICY = config.get('sc1', 'policy')
        # self.SILENT_MOD = config.getboolean('sc1', 'silent_mod')
        # self.DRY_RUN_MOD = config.getboolean('sc1', 'dry_run_mod')

        self.TRASH_PATH = '/home/maxim/PycharmProjects/Labiwe2/trash'
        self.INFO_PATH = self.TRASH_PATH + '/info'
        self.FILES_PATH = self.TRASH_PATH + '/files'
        self.MAX_SIZE = 11000
        self.MAX_COUNT = 15
        self.POLICY = 'time'
        self.SILENT_MOD = False
        self.DRY_RUN_MOD = False

        self.make_dir(self.TRASH_PATH)
        self.make_dir(self.INFO_PATH)
        self.make_dir(self.FILES_PATH)

        Checker.update(self.TRASH_PATH)

        # if self.SILENT_MOD:
        #     logging.setlevel(logging.CRITICAL)

    @staticmethod
    def make_dir(path):
        try:
            os.mkdir(path)
        except OSError:
            pass

    def dry_run_switcher(self):
        self.DRY_RUN_MOD = not self.DRY_RUN_MOD

    def silent_switcher(self):
        self.SILENT_MOD = not self.SILENT_MOD

    def policy_switcher(self):
        self.POLICY = raw_input()

    def policy_clean(self, count=10):
        Policy.delete_by_policy(self, count, self.POLICY)

    @logger
    def delete_file(self, dir_path, files):
        # if Checker.check_combine(dir_path, files, self):
        #     return "files %s deleted away" % files

        for file_name in files:
            folders, file_name = os.path.split(file_name)

            path = os.path.join(dir_path if folders == '' else folders, file_name)

            if os.path.lexists(path):
                if not Checker.trash_check(self.FILES_PATH, path):
                    if not self.DRY_RUN_MOD:
                        if Checker.check_file(self.FILES_PATH, file_name):
                            i = 1
                            while Checker.check_file(self.FILES_PATH, "{0}.{1}".format(file_name, i)):
                                i += 1
                            file_name = "{0}.{1}".format(file_name, i)

                        new_path = os.path.join(self.FILES_PATH, file_name)
                        with open(os.path.join(self.INFO_PATH, file_name + ".file_info"), "w") as file_info:
                            file_info.write(os.path.join(path) + "\n")
                            file_info.write(str(datetime.datetime.now()))

                        shutil.move(path, new_path)

                    return "file %s successful deleted" % file_name

                else:
                    raise Exception("file %s is already in trash" % file_name)
                    # raise Exception
            else:
                raise OSError("Don't found file or directory %s" % file_name)
                # raise OSError

    @logger
    def restore_file(self, file_names):
        for file_name in file_names:
            old_path = os.path.join(self.FILES_PATH, file_name)
            if os.path.lexists(old_path):
                with open(os.path.join(self.INFO_PATH, file_name + ".file_info"), "r") as file_info:
                    new_path = file_info.readline().strip("\n")
                    if os.path.exists(new_path):
                        print "file already created. press Y or N for replace"
                        if Checker.question():
                            if not self.DRY_RUN_MOD:
                                shutil.move(old_path, new_path)
                                os.remove(os.path.join(self.INFO_PATH, file_name + ".file_info"))
                            return "file %s successful replaced" % file_name

                    else:
                        if not self.DRY_RUN_MOD:
                            shutil.move(old_path, new_path)
                            os.remove(os.path.join(self.INFO_PATH, file_name + ".file_info"))
                        return "file %s successful restored" % file_name
            else:
                raise OSError("file %s don't found" % file_name)
                # raise OSError

    @logger
    def remove_from_trash(self, file_name):
        # for file_name in files_name:
        file_path = os.path.join(self.FILES_PATH, file_name)
        info_path = os.path.join(self.INFO_PATH, file_name + ".file_info")
        if os.path.lexists(file_name):
            if not self.DRY_RUN_MOD:
                if os.path.isdir(file_path):
                    shutil.rmtree(file_path)
                    os.remove(info_path)
                else:
                    os.remove(file_path)
                    os.remove(info_path)
            return "%s successful removed from trash" % file_name
        else:
            raise OSError("%s don't founded" % file_name)
            # raise Exception("ne rabotaet")

    @logger
    def clean_trash(self):
        if not self.DRY_RUN_MOD:
            shutil.rmtree(self.INFO_PATH)
            shutil.rmtree(self.FILES_PATH)
            self.make_dir(self.INFO_PATH)
            self.make_dir(self.FILES_PATH)
        return "trash successful cleaned"

    @logger
    def look_trash(self):
        files = os.listdir(self.INFO_PATH)
        for file_something in files:
            file_name = os.path.join(self.INFO_PATH, file_something)
            with open(file_name, "r") as file_info:
                file_path = file_info.readline()
            print ".".join(file_something.split(".")[:-1]), "\t", file_path
        return "trash looked"

    def remove_by_re(self, path, re_ex):
        files_or_dirs = os.listdir(path)
        for file_or_dir in files_or_dirs:
            if re.search(re_ex, file_or_dir):
                self.delete_file(path, [file_or_dir])
            else:
                if os.path.isdir(os.path.join(path, file_or_dir)):
                    self.remove_by_re(os.path.join(path, file_or_dir), re_ex)

    def restore_by_re(self, re_ex):
        files_or_dirs = os.listdir(self.FILES_PATH)
        for file_or_dir in files_or_dirs:
            if re.search(re_ex, file_or_dir):
                self.restore_file([file_or_dir])

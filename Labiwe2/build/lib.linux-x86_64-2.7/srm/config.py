import ConfigParser
import json


class Conf(object):

    @staticmethod
    def init_config():
        config = ConfigParser.RawConfigParser()

        config.add_section('sc1')
        config.set('sc1', 'trash_path', '/home/maxim/PycharmProjects/Labiwe2/trash')
        config.set('sc1', 'max_size', 11000)
        config.set('sc1', 'max_count', 15)
        config.set('sc1', 'policy', "time")
        config.set('sc1', 'silent_mod', False)
        config.set('sc1', 'dry_run_mod', False)

        json_conf = dict()
        json_conf['trash_path'] = '/home/maxim/PycharmProjects/Labiwe2/trash'
        json_conf['max_size'] = 11000
        json_conf['max_count'] = 15
        json_conf['policy'] = "time"
        json_conf['silent_mod'] = False
        json_conf['dry_run_mod'] = False

        with open('config.cfg', 'wb') as config_file:
            config.write(config_file)

        with open('config.json', 'wb') as json_config:
            json.dump(json_conf, json_config, indent=0)

    @staticmethod
    def make_config():
        config = ConfigParser.RawConfigParser()
        json_conf = dict()

        config.add_section('sc1')
        trash_path = raw_input("Enter trash_path or skip(press ENTER) for default value")
        if trash_path == '':
            config.set('sc1', 'trash_path', '/home/maxim/PycharmProjects/Labiwe2/trash')
            json_conf['trash_path'] = '/home/maxim/PycharmProjects/Labiwe2/trash'
        else:
            config.set('sc1', 'trash_path', trash_path)
            json_conf['trash_path'] = trash_path
        max_size = raw_input("Enter max_size or skip(press ENTER) for default value")
        if max_size == '':
            config.set('sc1', 'max_size', 11000)
            json_conf['max_size'] = 11000
        else:
            config.set('sc1', 'max_size', max_size)
            json_conf['max_size'] = max_size
        max_count = raw_input("Enter max_count or skip(press ENTER) for default value")
        if max_count == '':
            config.set('sc1', 'max_count', 15)
            json_conf['max_count'] = 15
        else:
            config.set('sc1', 'max_count', max_count)
            json_conf['max_count'] = max_count
        policy = raw_input("Enter policy(time, size, name) or skip(press ENTER) for default value")
        if policy == '':
            config.set('sc1', 'policy', "time")
            json_conf['policy'] = "time"
        else:
            config.set('sc1', 'policy', policy)
            json_conf['policy'] = policy
        silent_mod = raw_input("Enter silent_mod(True, False) or skip(press ENTER) for default value")
        if silent_mod == '':
            config.set('sc1', 'silent_mod', False)
            json_conf['silent_mod'] = False
        else:
            config.set('sc1', 'silent_mod', silent_mod)
            json_conf['silent_mod'] = silent_mod
        dry_run_mod = raw_input("Enter dry_run_mod(True, False) or skip(press ENTER) for default value")
        if dry_run_mod == '':
            config.set('sc1', 'dry_run_mod', False)
            json_conf['dry_run_mod'] = False
        else:
            config.set('sc1', 'dry_run_mod', dry_run_mod)
            json_conf['dry_run_mod'] = dry_run_mod

        with open('config.cfg', 'wb') as config_file:
            config.write(config_file)

        with open('config.json', 'wb') as json_config:
            json.dump(json_conf, json_config, indent=0)

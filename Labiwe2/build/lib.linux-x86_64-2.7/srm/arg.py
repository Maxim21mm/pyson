import argparse
from trash import Trash
import os
from config import Conf


def main():
    t = Trash()

    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--look", action="store_true", required=False)
    parser.add_argument("-c", "--clean", action="store_true", required=False)
    parser.add_argument('-d', "--delete", nargs="+", action="store", help="deletion", required=False)
    parser.add_argument('-dr', "--remove_by_re", action="store", help="deletion by regex", required=False)
    parser.add_argument('-r', "--restore", nargs="+", action="store", help="restoring files", required=False)
    parser.add_argument('-dro', "--dry_run_on", action="store_true", help="dry run mod on", required=False)
    parser.add_argument('-so', "--silent_on", action="store_true", help="silent mod on", required=False)
    parser.add_argument('-dt', "--delete_from_trash", #nargs="+",
                        action="store", help="delete files from trash", required=False)
    parser.add_argument('-pc', "--policy_clean", action="store_true", help="cleaning trash by policy", required=False)
    parser.add_argument('-dc', "--default_config", action="store_true", help="use default config", required=False)
    parser.add_argument('-cc', "--custom_config", action="store_true", help="use custom config", required=False)

    args = parser.parse_args()

    if args.default_config:
        Conf.init_config()

    if args.custom_config:
        Conf.make_config()

    if args.dry_run_on:
        t.dry_run_switcher()

    if args.silent_on:
        t.silent_switcher()

    if args.look:
        t.look_trash()

    if args.delete_from_trash is not None:
        t.remove_from_trash(args.delete_from_trash)

    if args.clean:
        t.clean_trash()

    if args.policy_clean:
        t.policy_clean()

    if args.remove_by_re is not None:
        t.remove_by_re(os.getcwd(), args.remove_by_re)

    if args.delete is not None:
        t.delete_file(os.getcwd(), args.delete)

    if args.restore is not None:
        t.restore_file(args.restore)

if "__main__" == __name__:
    main()

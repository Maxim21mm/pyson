import os
import shutil
import re


class Checker(object):

    @staticmethod
    def check_file(trash_path, file_name):
        path = os.path.join(trash_path, file_name)
        return os.path.exists(path)

    @staticmethod
    def check_secure(file_path):
        return os.access(file_path, os.W_OK)

    @staticmethod
    def in_trash(trash_path, file_path):
        folders = os.path.split(file_path)[0]
        if folders == trash_path:
            print "%s already in trash" % os.path.split(file_path)[1]
            return True
        else:
            return False

    @staticmethod
    def contain_trash(trash_path, file_path):
        if os.path.isdir(file_path) and re.match(file_path, trash_path):
            return True
        else:
            return False

    @staticmethod
    def trash_check(trash_path, file_path):
        first_check = Checker.in_trash(trash_path, file_path)
        second_check = Checker.contain_trash(trash_path, file_path)
        if first_check or second_check:
            return True
        else:
            return False

    @staticmethod
    def update(trash_path):
        files_path = os.path.join(trash_path, "files")
        info_path = os.path.join(trash_path, "info")
        list_files = os.listdir(files_path)
        list_info = os.listdir(info_path)
        checked_list = []

        for trash_file in list_files:
            for info_file in list_info:
                if trash_file + ".file_info" == info_file:
                    checked_list.append(trash_file)

        for trash_file in list_files:
            tmp = 0
            for checked in checked_list:
                if trash_file == checked:
                    tmp = 1
            if tmp == 0:
                if os.path.isdir(os.path.join(files_path, trash_file)):
                    print "this directory lost his info. Directory %s deleted from trash" % trash_file
                    shutil.rmtree(os.path.join(files_path, trash_file))
                else:
                    print "this file lost his info. File %s deleted from trash" % trash_file
                    os.remove(os.path.join(files_path, trash_file))

        for trash_info in list_info:
            tmp = 0
            for checked in checked_list:
                if trash_info == checked + ".file_info":
                    tmp = 1
            if tmp == 0:
                print "this info lost his file or directory. File_info %s deleted from INFO" % trash_info
                os.remove(os.path.join(info_path, trash_info))

    @staticmethod
    def check_size(dir_path, files, trash=None):
        if os.path.getsize(trash.FILES_PATH) > trash.MAX_SIZE:
            trash.clean_trash()
        files = Checker.list_checker(dir_path, files)
        trash_size = os.path.getsize(trash.FILES_PATH)
        files_size = 0
        for a_file in files:
            file_size = os.path.getsize(os.path.join(dir_path, a_file))
            files_size += file_size
        free_space = trash.MAX_SIZE - trash_size
        if free_space > files_size:
            return True
        elif files_size < trash.MAX_SIZE:
            while free_space < files_size:
                trash.policy_clean(1)
                free_space = trash.MAX_SIZE - os.path.getsize(trash.FILES_PATH)
            return True
        else:
            return False

    @staticmethod
    def check_count(dir_path, files, trash=None):
        if len(os.listdir(trash.FILES_PATH)) > trash.MAX_COUNT:
            trash.clean_trash()
        files = Checker.list_checker(dir_path, files)
        trash_count = len(os.listdir(trash.FILES_PATH))
        free_count = trash.MAX_COUNT - trash_count
        if len(files) <= free_count:
            return True
        elif len(files) < trash.MAX_COUNT:
                count = len(files) - free_count
                trash.policy_clean(count)
                return True
        else:
            return False

    @staticmethod
    def question():
        answer = raw_input()
        if answer == "Y":
            return True
        else:
            return False

    @staticmethod
    def question(trash=None):
        if trash.FORCE:
            return True
        else:
            answer = raw_input()
            if answer == "Y":
                return True
            else:
                return False

    @staticmethod
    def list_checker(dir_path, files, trash=None):
        tmp_list = []
        for file_name in files:
            folders, file_name = os.path.split(file_name)

            path = os.path.join(dir_path if folders == '' else folders, file_name)
            if (dir_path if folders == '' else folders) == trash.FILES_PATH:
                raise OSError("%s already in trash" % file_name)
            elif os.path.exists(path):
                tmp_list.append(path)
        return tmp_list

    @staticmethod
    def check_combine(dir_path, files, trash=None):
        if not Checker.check_count(dir_path, files, trash) or not Checker.check_size(dir_path, files, trash):
            print "this more, then my trash. delete away?"
            if Checker.question():
                for file_name in files:
                    folders, file_name = os.path.split(file_name)
                    path = os.path.join(dir_path if folders == '' else folders, file_name)
                    if os.path.isdir(path):
                        shutil.rmtree(path)
                    else:
                        os.remove(path)
                return True
            else:
                return True
        return False



import argparse
from trash import Trash
import os
from maxonrm.config import Conf
import ConfigParser
import logging
import sys


def main():
    err = False

    config = ConfigParser.RawConfigParser()
    config.read("/home/maxim/PycharmProjects/config.cfg")

    trash_path = config.get('sc1', 'trash_path')
    max_size = config.getint('sc1', 'max_size')
    size = config.getint('sc1', 'size')
    policy = config.get('sc1', 'policy')
    restore_policy = config.get('sc1', 'restore_policy')
    mode = config.get('sc1', 'mode')
    date = config.get('sc1', 'date')
    #
    # print trash_path, max_size, max_count, policy, silent_mod, dry_run

    t = Trash(trash_path, max_size, size, policy, mode, date, restore_policy)

    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--look", action="store_true", required=False)
    parser.add_argument("-c", "--clean", action="store_true", required=False)
    parser.add_argument('-d', "--delete", nargs="+", action="store", help="deletion", required=False)
    parser.add_argument('-dr', "--remove_by_re", action="store", help="deletion by regex", required=False)
    parser.add_argument('-r', "--restore", nargs="+", action="store", help="restoring files", required=False)
    parser.add_argument('-dron', "--dry_run_on", action="store_true", help="dry run mod on", required=False)
    parser.add_argument('-son ', "--silent_on", action="store_true", help="silent mod on", required=False)
    parser.add_argument('-dt', "--delete_from_trash", nargs="+",
                        action="store", help="delete files from trash", required=False)
    parser.add_argument('-dc', "--default_config", action="store_true", help="use default config", required=False)
    parser.add_argument('-cc', "--custom_config", action="store_true", help="use custom config", required=False)

    args = parser.parse_args()

    if args.default_config:
        Conf.init_config()

    if args.custom_config:
        Conf.make_config()

    if args.dry_run_on:
        t.dry_run_switcher()

    #
    #
    if t.DRY_RUN_MOD:
        logging.basicConfig(level=logging.INFO, format='_DRY_RUN_MOD_ON_ -%(message)s')
    else:
        logging.basicConfig(filename='/home/maxim/log.txt', level=logging.INFO,
                            format='%(asctime)s - %(levelname)s - %(message)s')
    log = logging.getLogger()
    #
    #

    if args.silent_on:
        log.setLevel(logging.CRITICAL)

    if args.look:
        t.look_trash()
        logging.info('trash looked')

    if args.delete_from_trash is not None:
        for file_or_dir in args.delete_from_trash:
            try:
                t.remove_from_trash(file_or_dir)
                logging.info('%s removed from trash', file_or_dir)
            except Exception:
                err = True
                logging.exception("Failed to remove from trash")

    if args.clean:
        t.clean_trash()
        logging.info('trash cleaning')

    if args.remove_by_re is not None:
        t.remove_by_re(os.getcwd(), args.remove_by_re)
        logging.info('in %s deleted by regex %s', os.getcwd(), args.remove_by_re)

    if args.delete is not None:
        for file_or_dir in args.delete:
            try:
                t.delete_file(os.getcwd(), file_or_dir)
                logging.info('%s successful placed into trash', file_or_dir)
            except Exception as e:
                err = True
                logging.error(e)

    if args.restore is not None:
        for file_or_dir in args.restore:
            try:
                t.restore_file(file_or_dir)
                logging.info('%s successful restored from trash', file_or_dir)
            except Exception as e:
                err = True
                logging.error(e)

    return err

if "__main__" == __name__:
    err = main()
    if err:
        sys.exit(1)

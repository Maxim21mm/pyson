import os


def list_for_autodelete_by_time(trash=None):
    for_del = []
    trash_files_and_dirs = os.listdir(trash.FILES_PATH)
    for file_or_dir in trash_files_and_dirs:
        with open(os.path.join(trash.INFO_PATH, file_or_dir + ".file_info"), "r") as file_info:
            new_path = file_info.readline().strip("\n")
            time = file_info.readline()
        if time < trash.DATE:
            #trash.remove_from_trash(file_or_dir)
            for_del.append(file_or_dir)

    return for_del

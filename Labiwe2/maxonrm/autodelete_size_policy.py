import os


def list_for_autodelete_by_size(trash=None):
    for_del = []
    trash_files_and_dirs = os.listdir(trash.FILES_PATH)
    for file_or_dir in trash_files_and_dirs:
        size = os.path.getsize(os.path.join(trash.FILES_PATH, file_or_dir))
        # print size
        if size > trash.SIZE:
            #trash.remove_from_trash(file_or_dir)
            for_del.append(file_or_dir)

    return for_del

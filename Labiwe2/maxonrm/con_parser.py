import ConfigParser
import argparse
import json
import os


def main():
    config = ConfigParser.RawConfigParser()
    parser = argparse.ArgumentParser()
    parser.add_argument('-tp', '--trash_path', help='path of trash')
    parser.add_argument('-s', '--size', help='maximal size of trash')
    parser.add_argument('-ms', '--max_size', help='maximal size of file in trash.')
    parser.add_argument('-da', '--date', help='maximal time for autodelete files in trash')
    parser.add_argument('-po', '--policy', help='autodelete policy(time or size)')
    parser.add_argument('-rp', '--restore_policy', help='restore policy(replace or ask)')
    parser.add_argument('-m', '--mode', help='mode of working(force, interactive')
    parser.add_argument('-l', '--list', action='store_true', help='list config')

    args = parser.parse_args()

    json_dict = {}
    config_path = '~/PycharmProjects'
    if args.list:
        with open(os.path.join(os.path.expanduser(config_path), 'config.cfg'), 'r+') as configfile:
            for line in configfile.readlines():
                print line
    else:
        try:
            config.add_section('sc1')
        except:
            pass

        if args.trash_path:
            config.set('sc1', 'trash_path', args.trash_path)
            json_dict['trash_path'] = args.trash_path
        else:
            config.set('sc1', 'trash_path', '/home/maxim/PycharmProjects/TRASH')
            json_dict['trash_path'] = '/home/maxim/PycharmProjects/TRASH'

        if args.size:
            config.set('sc1', 'size', args.size)
            json_dict['size'] = args.size
        else:
            config.set('sc1', 'size', 1500000)
            json_dict['size'] = 1500000

        if args.max_size:
            config.set('sc1', 'max_size', str(args.max_size))
            json_dict['max_size'] = args.max_size
        else:
            config.set('sc1', 'max_size', 15)
            json_dict['max_size'] = 15

        if args.date:
            config.set('sc1', 'date', args.date)
            json_dict['date'] = args.date
        else:
            config.set('sc1', 'date', '2017-04-22 02:48:02.020848')
            json_dict['date'] = '2017-04-22 02:48:02.020848'

        if args.policy:
            config.set('sc1', 'policy', args.policy)
            json_dict['policy'] = args.policy
        else:
            config.set('sc1', 'policy', 'size')
            json_dict['policy'] = 'size'

        if args.restore_policy:
            config.set('sc1', 'restore_policy', args.restore_policy)
            json_dict['restore_policy'] = args.restore_policy
        else:
            config.set('sc1', 'restore_policy', 'replace')
            json_dict['restore_policy'] = 'replace'

        if args.mode:
            config.set('sc1', 'mode', args.mode)
            json_dict['mode'] = args.mode
        else:
            config.set('sc1', 'mode', 'force')
            json_dict['mode'] = 'force'

        with open(os.path.join(os.path.expanduser(config_path), 'config.cfg'), 'wb') as configfile:
            config.write(configfile)
        with open(os.path.join(os.path.expanduser(config_path),' config.json'), 'wb') as config_json:
            json.dump(json_dict, config_json, indent=0)

if __name__ == '__main__':
    main()